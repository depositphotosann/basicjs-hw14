"use strict"

/*
Теоретичні питання
1. В чому полягає відмінність localStorage і sessionStorage?
localStorage і sessionStorage - це два об'єкти веб-сховища, які надають зручний спосіб зберігати дані на клієнтському боці при використанні JavaScript. Основна відмінність між ними полягає в тому, як довго зберігаються дані і чи діляться вони між вкладками браузера.

1) localStorage:
Дані зберігаються між сесіями та вкладками браузера.
Дані зберігаються на довший термін (поки не буде явно видалено користувачем або сценарієм).
Доступ до даних може бути отриманий навіть після перезапуску браузера або комп'ютера.
Дані доступні на одному домені і не переходять на інші домени через політику сховища.
// Приклад збереження у localStorage
localStorage.setItem('key', 'value');

2) sessionStorage:
Дані зберігаються лише протягом часу життя сесії браузера.
Дані видаляються при закритті вкладки або браузера.
Доступ до даних неможливий після перезапуску браузера або комп'ютера.
Як правило, обмежений однією вкладкою або вікном браузера.
// Приклад збереження у sessionStorage
sessionStorage.setItem('key', 'value');

Обидва методи localStorage і sessionStorage використовують подібний API для збереження та отримання даних:
setItem(key, value): Додає новий ключ і значення до сховища.
getItem(key): Повертає значення, пов'язане з ключем.
removeItem(key): Видаляє пару ключ-значення за ключем.
clear(): Видаляє всі дані з сховища для даного домена.
Обираючи між localStorage і sessionStorage, важливо враховувати тривалість потрібного зберігання даних та обсяг інформації, яку ви хочете зберігати.

2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
Збереження чутливої інформації, зокрема паролів, у localStorage чи sessionStorage має свої ризики, і важливо враховувати аспекти безпеки для захисту цих даних. Нижче подано деякі поради з безпеки:

1) Ніколи не зберігайте паролі в чистому вигляді:
Завжди використовуйте безпечний метод для збереження паролів, такий як хешування (наприклад, з використанням bcrypt або SHA-256). Зберігання паролів у відкритому вигляді може призвести до серйозних наслідків при потраплянні зловмисникам.
2) Використовуйте HTTPS:
Завжди використовуйте захищене з'єднання (HTTPS), особливо при обробці чутливої інформації. HTTPS забезпечує шифрування даних, яке ускладнює зловмисникам отримання доступу до пересиланих даних.
3) Застосовуйте політики same-origin:
Спробуйте обмежити доступ до localStorage або sessionStorage за допомогою політик same-origin. Це допомагає запобігти доступу до сховища з інших доменів.
4) Обмежте об'єм даних:
Не зберігайте занадто багато інформації в сховищі. Якщо дані необхідні тільки для одного сеансу, використовуйте sessionStorage, інакше розгляньте можливість короткочасного зберігання в localStorage.
5) Завжди перевіряйте валідність даних:
Перевіряйте дані, які ви отримуєте з localStorage чи sessionStorage, на валідність та відповідність вашим очікуванням.
6) Не передавайте конфіденційну інформацію через URL:
Уникайте передачі конфіденційної інформації через URL, оскільки це може потрапити до логів сервера та історії браузера.
7) Завжди очищуйте дані після використання:
Після завершення роботи з чутливою інформацією очистіть її з localStorage чи sessionStorage. Це зменшить ризик втрати даних внаслідок несанкціонованого доступу.
Загалом, важливо враховувати, що використання localStorage чи sessionStorage не є найбільш безпечним способом для зберігання чутливої інформації. Якщо це можливо, рекомендується використовувати серверні механізми для обробки та зберігання конфіденційних даних.

3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
Коли завершується сеанс браузера, дані, збережені в sessionStorage, будуть автоматично видалені. sessionStorage призначений для тимчасового зберігання даних, пов'язаних із поточним сеансом користувача. 
Коли користувач закриває вікно браузера або завершує сеанс, дані в sessionStorage видаляються і не зберігаються між сеансами.
Такий механізм гарантує, що інформація, яку ви зберігаєте в sessionStorage, буде доступною тільки під час активного використання користувачем відкритого вікна браузера. 
Якщо користувач відкриває нове вікно чи виконує перезавантаження сторінки, sessionStorage очищується, і дані стають недоступними.
Це робить sessionStorage відмінним від localStorage, де дані зберігаються назавжди або до їхнього видалення вручну. localStorage зберігає дані на користь майбутніх сеансів користувача.
*/

/*
Практичне завдання:
Реалізувати можливість зміни колірної теми користувача.
Технічні вимоги:
- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.
Примітки:
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.
Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/

    const themeButton = document.querySelector('.change-theme');
    const savedTheme = localStorage.getItem('theme');

    function applyTheme(theme) {
        document.body.classList.toggle('dark-theme', theme === 'dark');
    }

    if (savedTheme) {
        applyTheme(savedTheme);
    }

    themeButton.addEventListener('click', () => {
        const currentTheme = document.body.classList.contains('dark-theme') ? 'blue' : 'dark';
        applyTheme(currentTheme);

        localStorage.setItem('theme', currentTheme);
    });